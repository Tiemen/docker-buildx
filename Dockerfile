ARG DOCKER_VERSION=19.03.8
FROM docker:$DOCKER_VERSION

ARG DOCKER_HOST=tcp://docker:2375
ARG DOCKER_DRIVER=overlay2
ARG DOCKER_BUILDKIT=1
ARG DOCKER_CLI_EXPERIMENTAL=enabled
ARG BUILDX_URL=https://github.com/docker/buildx/releases/download/v0.3.1/buildx-v0.3.1.linux-amd64

COPY .docker ${HOME}/
COPY setup_builder.sh /bin/

RUN mkdir -p ${HOME}/.docker/cli-plugins/ && \
    wget -O ${HOME}/.docker/cli-plugins/docker-buildx ${BUILDX_URL} && \
    chmod a+x ${HOME}/.docker/cli-plugins/docker-buildx
